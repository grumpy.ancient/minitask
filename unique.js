const unique = (arr) => {
    return arr.filter((num, i) => arr.indexOf(num) === i);
}
