const intersection = (arr, ...arrs) => {
    return arr.filter(num => arrs.every(arr => arr.includes(num)));
}