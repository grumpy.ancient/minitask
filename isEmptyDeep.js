const isEmptyDeep = (obj) => {
    if (obj === null) {
        return true;
    }
    if (typeof obj === 'object') {
        const objectKeys = Object.keys(obj);
        if (objectKeys.length === 0) {
            return true;
        }

        let result;
        for (let i = 0; i < objectKeys.length; i += 1) {
            const value = obj[objectKeys[i]];
            if (typeof value === 'boolean' || (typeof value === 'number' && !Number.isNaN(value))
                || (typeof value === 'string' && value !== '')) {
                result = false;
                break;
            }
            result = true;
        }
        return result;
    }
};